
library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FP_MULTIPLIER is
port( 	fp_a 	  : in std_logic_vector(31 downto 0);
	fp_b 	  : in std_logic_vector(31 downto 0);
	clk 	  : in std_logic;
	en 	  : in std_logic;
	zero 	  : out std_logic;
	overflow  : out std_logic;
	underflow : out std_logic;
	fp_result : out std_logic_vector(31 downto 0));
end entity FP_MULTIPLIER;

architecture behav of FP_MULTIPLIER is begin
	process(clk, fp_a, fp_b, en)

		variable a_sign, b_sign : std_logic; 
		variable a_exp, b_exp: std_logic_vector(7 downto 0 );
		variable expSum : std_logic_vector( 8 downto 0 );
		variable a_manti, b_manti: std_logic_vector(22 downto 0);
		variable mult_manti_temp : std_logic_vector( 47 downto 0 );
		variable add_exp_temp : std_logic_vector(8 downto 0);
		variable product : std_logic_vector(47 downto 0);
		variable CarryOut : std_logic;
		variable a_manti_m : std_logic_vector(23 downto 0);
		variable temp : std_logic_vector( 24 downto 0 );
		variable A, Q, M : std_logic_vector( 23 downto 0 );
		variable sumShift : std_logic_vector( 48 downto 0 );
		variable round : std_logic_vector(1 downto 0);
	begin
	if clk='0' and en='1' and clk'event then 
		a_sign := fp_a(31);
		b_sign := fp_b(31);
		a_exp := fp_a( 30 downto 23 );
		b_exp := fp_b( 30 downto 23 );
		a_manti := fp_a( 22 downto 0 );
		b_manti := fp_b( 22 downto 0 );
		
		product( 47 downto 24 ) := (others=>'0');
		M := '1' & b_manti;
		sumShift(48) := '0' ;--initalize carry
		sumShift(47 downto 24) := (others=>'0'); --initialize A
		sumShift(23 downto 0) := '1' & a_manti; --Initialize Q
		
		for I in 0 to 23 loop
			CarryOut := sumShift(48);
			A := sumShift(47 downto 24);
			Q := sumShift(23 downto 0);
			if (Q(0) = '1') then 
				temp := std_logic_vector(unsigned('0' & A) + unsigned(M));
				sumShift(48) := temp(24);
				sumShift(47 downto 24) := temp(23 downto 0);
				sumShift(23 downto 0) := Q;
			end if;
			sumShift := '0' & sumShift(48 downto 1);
		end loop;
	end if;
	if clk='1' and en='1' and clk'event then
		if (unsigned(a_exp) = 255 and unsigned(b_exp) = 0) or (unsigned(a_exp) = 0 and unsigned(b_exp) = 255) then 
			fp_result(31 downto 0) <= (others => 'U');
			overflow <= '0';
			underflow <= '0';
		elsif unsigned(a_exp) = 255 or unsigned(b_exp) = 255 then --multiply inf
			fp_result(31) <= a_sign xor b_sign;
			fp_result(30 downto 23) <= "11111111";
			fp_result(22 downto 0) <= (others => '0');
			overflow <= '1';
			underflow <= '0';
		elsif (unsigned(a_exp) = 0 or unsigned(b_exp) = 0) then --multiply 0
			fp_result(31) <= '0';
			fp_result(30 downto 23) <= "00000000";
			fp_result(22 downto 0) <= (others => '0');
			overflow <= '0';
			underflow <= '0';
		else
			product := sumShift(47 downto 0);
			if (product(47) = '1') then
				fp_result(22 downto 0) <= product(46 downto 24);
				round := "01";
			else
				fp_result(22 downto 0) <= product(45 downto 23);
				round := "00";
			end if;
			expSum := std_logic_vector(unsigned('0' & a_exp) + unsigned('0' & b_exp) + unsigned(round) - 127 );
			if (expSum(8)='1' and expSum(7)='0') then 
				fp_result(30 downto 23) <= "11111111";
				fp_result(22 downto 0) <= (others => '0');
				overflow <= '1';
				underflow <= '0';
					zero <='0';
			elsif (expSum(8)='1' and expSum(7)='1') then 				
				fp_result(30 downto 23) <= "00000000";
				fp_result(22 downto 0) <= (others => '0');
				overflow <= '0';
				underflow <= '1';
				zero <='0';
			else								  	
				fp_result(30 downto 23) <= expSum(7 downto 0);
				overflow <= '0';
				underflow <= '0';
				zero <='0';
			end if;
			fp_result(31) <= a_sign xor b_sign;
		end if;
	end if;
end process;
end behav;